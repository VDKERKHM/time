package time.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import time.service.TimeService;

@RestController
public class TimeController {

    @Autowired
    private TimeService service;

    @RequestMapping(value = "/time", method = RequestMethod.GET)
    public String time() {
        return service.getCurrentTime().toString();
    }
}
