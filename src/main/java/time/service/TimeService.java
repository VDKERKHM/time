package time.service;

import java.time.LocalDateTime;

public interface TimeService {

    LocalDateTime getCurrentTime();
}
