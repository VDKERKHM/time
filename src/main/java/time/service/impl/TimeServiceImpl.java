package time.service.impl;

import org.springframework.stereotype.Service;
import time.service.TimeService;

import java.time.LocalDateTime;

@Service
public class TimeServiceImpl implements TimeService {

    @Override
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }
}
